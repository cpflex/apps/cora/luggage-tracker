![Cora Logo](sdk/img/cora-logo.png)
# Cora Luggage Tracker Application 
**Target: CT100X, CT102X Series Devices**<br/>
**Version: 1.000**<br/>
**Release Date: 2023/08/24**

Luggage tracking application providing BLE/WiFi location tracking when in coverage and only high-frequency pings when out of coverage.

Useful for luggage tracking applications to provide precise location when in coverage and a ping to support coarse location when out of coverage, increasing battery life
and reducing logging of data when out of network.

The application provides downlink commands to configure location, tracking, and general system functions.  [See Cora Luggage Tracker Protocol Schema for more information](./appapi/luggage-tracker.ocm.json).

## Key Features  

3. Motion activated WiFi/BLE location report when moving and stop points.
4. Charging, discharging, and battery status (every 5%) reports.
5. Battery Life TBD (> 6 months).
6. Battery status indicator
7. Sends initial location message immediately after reset.
 
# User Interface 

This application is meant to be used with both the Flex Tracker (CT100X) and Micro Tracker (CT102X) series of tags.   Certain UI capabilities will vary depending upon the device support. 

## Button Descriptions  (CT1000 only)

The following table summarizes the user interface for this application.  See the sections
below for more detailed information regarding each button action.  


| **Command**                         |     **Action**       |     **Button**     |  **Description**           |
|-------------------------------------|:--------------------:|:------------------:|---------------------------|
| **Locate Now**                      | single (1) <br>quick press        | button #1          | Initiates an immediate location request  |  
| **Battery Status**                  | single (1) <br>quick press        | button #2          | Indicates battery level <br> [*See Battery Indicator*](#battery-indicator)  |  
| **Network Reset**                   | hold > 15<br>seconds              | any button or both | Network reset device      |
| **Factory Reset**                   | hold > 25<br>seconds              | any button or both | Resets the network and configuration to factory defaults for the application.     |
 
## LED Descriptions

The following table indicates the meaning of the two bi-color LEDs on the device.  The LED's are
positioned:

* **LED #1** - Upper LED, furthest away from charger pads
* **LED #2** - Lower LED, closest to charger pads (CT100X series only)

| **Description**                |                **Indication**               |   **LEDS**  |
|--------------------------------|:-------------------------------------------:|:-----------:|
| **Device Reset**               |       Green blink <br>three (3) times       |     both    |
| **Locate Initiated**           |        Green blink <br>two (2) times        |    LED #1   |
| **Battery Charging**           | Orange slow blink every<br>ten (10) seconds |    LED #1   |
| **Battery Fully Charged**      |  Green slow blink every<br>ten (10) seconds |    LED #1   |
| **Invalid Input**              |  Red blink <br>three (3) times quickly      |    LED #1   |


See status indicators below for additional LED signaling when requesting battery status.
 
### Battery Indicator  (CT1000 only)

Quickly pressing Button #2 will indicate the percent charge of the battery in 20% increments.

LED #2 will blink as follows to indicate percent charge.

| LED #2   | % Charge    | Description                                     |
|----------|-------------|-------------------------------------------------|
|  1 red   |   < 10%     | Battery level is critical,  charge immediately. |
|  1 green |  15% - 30%  | Battery low  less than 30% remaining.           |
|  2 green |  30% - 50%  | Battery is about 40% charged.                    |
|  3 green |  50% - 70%  | Battery is about 60% charged.                    |
|  4 green |  70% - 90%  | Battery  is about 80% charged.                   |
|  5 green |  > 90%      | Battery is fully charged.                       |


## Integration

### Traxmate integration
To work with the Traxmate application a custom device type is registered.  Set your device type to Cora Luggage Tracker (id=???).  Use this ID in the cloud stack device templates to pre-configure the device type.   

### Testing 
A postman collection is provided documenting all the downlink messages that can be sent to the Bike app. This can be found in the test folder.

---
*Copyright 2023, Codepoint Technologies, Inc.* 
*All Rights Reserved*
  

/**
 *  Name:  app.p
 *
 *  This module is the proprietary property of Codepoint Technologies, Inc.
 *  Copyright (C) 2023 Codepoint Technologies, Inc.
 *  All Rights Reserved
 */

 #include "app.i"
 #include "version.i" //Compile time generated file.

//System includes.
#include "cpflexver.i"

//Modules
#include "battery.i"
#include "TelemMgr.i"
#include "SysMgr.i"
#include "LocationConfig.i"
#include "TrackingEngine.i"

//Include Controller configuration.
#include "config/ConfigApp.i"


/*******************************************************
* Forward Declarations.
*******************************************************/
forward _ProcessSyslogCommand( value  );
forward  ResultCode: _ReportSyslogStatus( seqOut);
#pragma warning disable 213


/*******************************************************
* Constants and static variables.
*******************************************************/


/*******************************************************
* System events.
*******************************************************/

@SysInit()
{
	LogFmt(	
		"\r\n********** cora-luggage-app **************\r\n** Version:      %s\r\n** CP-Flex Ver:  %s\r\n** Build Date:   %s %s\r\n*********************************************",	
		MC_info, MP_med, LO_default, 
		__VERSION__, 
		CPFLEX_VERSION, 
		__DATE__, 
		__TIME__
	);

#ifdef __SYSLOG__
//	//Test mode we automatically enable the System Log.
	SysSetLogFilters( MsgCategory: 0xFF, MsgCategory: 0xFF);
#endif 

	loccfg_Init();
	battery_Init();
	TelemMgr_Init();
	SysmgrInit();
	trk_Init();

}

/*******************************************************
* Event Handlers
*******************************************************/
@UiButtonEvent( idButton, ButtonPress: press, ctClicks)
{
	TraceDebugFmt( "Button: id=%d, press=%d, clicks=%d", idButton, press, ctClicks);
	new bool: bHandled = false;	
	switch(press) {
		case BP_short: {
			if( idButton == BTN1) {
				//bHandled = trk_Acquire();
			}else if (idButton == BTN2)	{
				bHandled = battery_IndicateChargeLevel();
			}
		}
	}

	//Signal user if failed.
	if( !bHandled ) UiSetLed( LED2, LM_count, 3, 150, 100 );	
}

#pragma warning disable 203

/**
* @brief  Application Defined callback handler that indicates entering power savings.
* @remarks Required implementation to catch enter power saving events.
*/
app_EnterPowerSaving(pctCharged)
{
	// Tracking is deactivated to preserve battery.	
	TraceDebug("Enter Power Save Mode");
	trk_EnterPowerSaving();
	battery_SendBatteryInfo();

}

/**
* @brief  Application Defined callback handler that indicates entering power savings.
* @remarks Required implementation to catch exit power saving events.
*/
app_ExitPowerSaving(pctCharged)
{
	TraceDebug("Exit Power Save Mode");
	battery_SendBatteryInfo();	
	trk_ExitPowerSaving();
}

/***
* @brief Callback event handler implemented by the application to receive
*   notification of state changes and signals enabled by the state definitions.
* @param id The callback event identifier.
* @param stateCur  The array containing the current state data. Array has structure
*  as follows:  [ .idxState, .tickSamples, .tickMotion, .userValue1, .userValue2,  .tStateChange, .ctMeasured, .tLastLocate].
* @return Returns true or false on evaluation callbacks otherwise returns true.
*/
bool: trk_CallbackEvent(  
	id,
	const stateCur[ .idxState, .tickSamples, .tickMotion, .userValue1, .userValue2, .tStateChange, .ctMeasured, .tLastLocate],
	const properties[]
) {
	//TODO
}


/*******************************************************
* Communication Implementation
*******************************************************/
#pragma warning enable 203


/**
* @brief Received data sequence handler.
* @remarks.  Processes received sequences and reports any requested. data.
*/
@TelemRecvSeq( Sequence: seqIn, ctMsgs )
{
	//Process the received sequence.
	new Message: msg;
	new MsgType: tmsg;
	new tcode;
	new id;
	new MsgPriority: priority;
	new MsgCategory: category;
	new ResultCode: rc;
	new int: ct= int: 0;
	new Sequence:seqOut;

	//Allocate a sequence to send messages.
	//Create a sequence to hold all potential commands.
	rc = TelemSendSeq_Begin( seqOut, 150); 
	if( rc != RC_success)
	{
		TraceError("Could not allocate sequence buffer, ");
	}
	
	//Process each of the received messages.
	while( rc == RC_success 
		&& TelemRecv_NextMsg( seqIn, msg, tmsg, tcode, id, priority, category) 
			== RC_success)
	{		
	 	//Process using Module Handlers.
		ct += battery_Downlink( seqOut, tmsg, id, msg);
		ct += SysmgrDownlink( seqOut, tmsg, id, msg);
		ct += TelemMgr_Downlink( seqOut, tmsg, id, msg);
		ct += loccfg_downlink( seqOut, tmsg, id, msg);
		ct += trk_Downlink( seqOut, tmsg, id, msg);
	}

	//Done with the input sequence.
	TelemRecv_SeqEnd( seqIn);

	//Either send or discard output sequence 
	//send only if there are messages to send and we were successful in creating them.
	TelemSendSeq_End( seqOut, rc != RC_success || ct == int:0, TPF_CONFIRM);
	if( rc != RC_success)
		TraceError("Cannot send sequence an error occurred while assembling.");
}



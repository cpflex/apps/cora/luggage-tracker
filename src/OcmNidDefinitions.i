/*******************************************************************************
*                !!! OCM CODE GENERATED FILE -- DO NOT EDIT !!!
*
* Cora Luggage Tracker Protocol Schema
*  nid:        cora-luggage-tracker
*  uuid:       83ec681d-d7bc-4ba3-9e48-c7548df9cbea
*  ver:        1.0.0.0
*  date:       2023-10-26T22:32:47.321Z
*  product: Cora Tracking CT10XX
* 
*  Copyright 2023 - Codepoint Technologies, Inc. All Rights Reserved.
* 
*******************************************************************************/
const OcmNidDefinitions: {
	NID_LocConfigPmode = 1,
	NID_SystemLogAll = 2,
	NID_LocConfigPmodeDefault = 3,
	NID_TrackConfig = 4,
	NID_SystemLogDisable = 5,
	NID_PowerBattery = 6,
	NID_LocConfigPtech = 7,
	NID_SystemReset = 8,
	NID_PowerChargerCritical = 9,
	NID_TrackConfigNomintvl = 10,
	NID_SystemConfigPollintvl = 11,
	NID_SystemLog = 12,
	NID_TrackMode = 13,
	NID_SystemLogAlert = 14,
	NID_LocConfigPmodePrecise = 15,
	NID_LocConfigPtechBle = 16,
	NID_LocConfigPtechWifi = 17,
	NID_TrackModeActive = 18,
	NID_LocConfig = 19,
	NID_LocConfigPmodeMedium = 20,
	NID_PowerCharger = 21,
	NID_LocConfigPmodeCoarse = 22,
	NID_TrackModeDisabled = 23,
	NID_TrackConfigEmrintvl = 24,
	NID_SystemErase = 25,
	NID_LocConfigPtechWifible = 26,
	NID_LocConfigPtechAutomatic = 27,
	NID_PowerChargerCharging = 28,
	NID_SystemLogInfo = 29,
	NID_TrackConfigAcquire = 30,
	NID_TrackConfigInactivity = 31,
	NID_SystemLogDetail = 32,
	NID_PowerChargerCharged = 33,
	NID_SystemConfig = 34,
	NID_PowerChargerDischarging = 35,
	NID_TrackModeEnabled = 36
};
